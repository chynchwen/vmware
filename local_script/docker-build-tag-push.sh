#!/usr/bin/env bash
cd ../flask-app
docker build -t flask-app-chyn .
docker image tag flask-app-chyn:latest chynchwen/flask-app-chyn
docker login
docker push chynchwen/flask-app-chyn
cd -