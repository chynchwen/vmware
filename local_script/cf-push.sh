#!/usr/bin/env bash
DOCKER_IMAGE=index.docker.io/chynchwen/flask-app-chyn:latest
cf7 push flask-app-chyn --docker-image=$DOCKER_IMAGE -u process --strategy rolling
cf7 routes
